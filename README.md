# WidgitCogs

Widgit is a next-generation hosting platform tailored for mods, modding guides,
and guilds which is currently under active development. At present, our hosting
platform is in private beta, but we have several active Discord servers running
under our brand today!

Each of our Discord servers is managed by a networked instance of the awesome
[Red DiscordBot](https://github.com/Cog-Creators/Red-DiscordBot) which has been
extended through a series of custom cogs we built in-house. Until now, those
cogs were all private and fairly hastily written. Now, we're working on refining
them for public release!

## Installation

Installing cogs from this repo is fairly straightforward. For the sake of
simplicity, we recommend installation using the official Red repository and cog
management commands.

```text
[p]repo add Widgit https://gitlab.com/widgitlabs/widgitcogs
[p]cog list Widgit
[p]cog install Widgit [cogname]
```

## Cogs

This repository should be considered a constant work-in-progress. While we want
to provide the best possible code to our users, it should be remembered that
much of this began as internal code which was _not_ intended to be high-quality;
in fact, it was almost all hacked together quickly. We are working hard to
bring our code up to at least our own standards, but it will take a while!

| Name | Status | Description | Notes |
| --- | --- | --- | --- |
| comedycorner | **Stable** | A collection of entertainment commands. | Intended to be a modular entertainment cog. |
| comedycornerlite | **Stable** | A collection of entertainment commands. | A stripped-down version of Comedy Corner for people who are using the predacogs randimage cog. |
| faqs | **Stable** | Lets you define a channel for and intelligently manage FAQs. | |
| suggestionbox | **WIP** | A flexible suggestion box. | |
| wordpress | **Stable** | Integrates with any site running WordPress and allows users to display information from the site based on a variety of criteria. | |

The following cogs are intended for internal use, or are expanded versions of
cogs we have forked which are still pending development.

| Name | Status | Description | Notes |
| --- | --- | --- | --- |
| devkit | **Stable** | It's my playground for testing things. | You definitely don't want to use this cog. |
| embededitor | **WIP** | Allows staff to post and edit embeds through a simple interactive system. | The way I wrote the WidgitEmbed class is horrible and buggy. Don't use this for now. |
| gallery | **Private** | Lets you define restricted gallery channels. | Forked from [saurichable](https://github.com/elijabesu/SauriCogs) |
| guide | **Stable** | A custom fork of our WordPress cog with additional data pulled from our modified API. | You probably don't want to use this cog. |
| seen | **Private** | Check when a user was last active on the server. | Forked from [aikaterna](https://github.com/aikaterna/aikaterna-cogs/) |
