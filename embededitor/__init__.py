from .core import EmbedEditor


def setup(bot):
    bot.add_cog(EmbedEditor(bot))
