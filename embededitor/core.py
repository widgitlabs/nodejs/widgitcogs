from discord import TextChannel
from redbot.core import commands, checks
from redbot.core.config import Config
from redbot.core.i18n import Translator, cog_i18n
from .widgitutils.embeds import editor


_ = Translator("EmbedEditor", __file__)


@cog_i18n(_)
class EmbedEditor(commands.Cog):
    """
    An interactive embed manager.
    """

    def __init__(self, bot):
        self._ = _
        self.bot = bot
        self.config = Config.get_conf(
            self, identifier=3870203096, force_registration=True
        )

        _defaults = {"embeds": {}}

        self.config.register_guild(**_defaults)

    @commands.group("embed")
    async def embed(self, ctx):
        """
        Manipulate embeds.
        """

    @embed.command(name="add")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    @checks.bot_has_permissions(manage_messages=True)
    async def add(self, ctx: commands.Context, channel: TextChannel = None):
        """
        Add an embed.
        """

        await editor(self, ctx, channel=channel, type="add", inline=True)

    @embed.command(name="edit")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    @checks.bot_has_permissions(manage_messages=True)
    async def edit(
        self,
        ctx: commands.Context,
        embed_id: str,
        channel: TextChannel = None,
        inline=False,
    ):
        """
        Edit an embed.
        """

        await editor(
            self, ctx, channel=channel, type="edit", embed_id=embed_id, inline=inline
        )
