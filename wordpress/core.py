import asyncio
from bs4 import BeautifulSoup
from discord import Embed
from redbot.core import commands, checks
from redbot.core.config import Config
from redbot.core.i18n import Translator, cog_i18n
from redbot.core.utils.menus import start_adding_reactions
from redbot.core.utils.predicates import ReactionPredicate
from time import strftime, strptime
from urllib.parse import quote_plus
from .utils import trailingslashit, remote_request


_ = Translator("WordPress", __file__)


@cog_i18n(_)
class WordPress(commands.Cog):
    """
    Provides integration with a WordPress-powered website.
    """

    api_base = "wp-json/wp/v2/"
    api_query = "?search="
    search_base = "?s="
    type_base = "&post_type="

    def __init__(self, bot):
        self.bot = bot
        self.config = Config.get_conf(
            self, identifier=3870203081, force_registration=True
        )

        _defaults = {
            "site_url": "",
            "post_type": "posts",
            "max_posts": 5,
            "show_date": True,
            "orderby": "relevance",
            "order": "desc",
        }

        self.config.register_guild(**_defaults)

    @commands.command("search")
    async def search(self, ctx: commands.Context, *, query: str):
        """
        Search the guide.
        """

        max_posts = await self.config.guild(ctx.guild).max_posts()
        post_type = await self.config.guild(ctx.guild).post_type()
        site_url = await self.config.guild(ctx.guild).site_url()
        show_date = await self.config.guild(ctx.guild).show_date()
        orderby = await self.config.guild(ctx.guild).orderby()
        order = await self.config.guild(ctx.guild).order()
        api_opts = f"&orderby={orderby}&order={order}"

        if not site_url:
            return await ctx.send(_("The site URL must be set first!"))

        site_url = trailingslashit(site_url)
        api_url = site_url + self.api_base + post_type + self.api_query + query
        api_url += api_opts
        search_url = site_url + self.search_base + quote_plus(query) + self.type_base + post_type

        response = await remote_request(api_url)

        try:
            more = False

            # Bail if no results found
            if len(response) <= 0:
                return await ctx.send(_("No results found for `{}`!").format(query))

            # Should we include a more link?
            max_posts = int(max_posts)
            if len(response) > max_posts:
                response = response[:max_posts]
                more = True

            for post in response:
                description = BeautifulSoup(post["content"]["rendered"])
                description = description.get_text()

                # Cut description to length
                if len(description) > 1500:
                    description = description[:1495].strip() + "[...]"

                footer = ""

                # Maybe add post date
                if show_date:
                    date = strptime(post["modified"], "%Y-%m-%dT%H:%M:%S")
                    footer = _("Modified: {}".format(strftime("%d %b %Y", date)))

                # Build embed
                embed = Embed(
                    title=post["title"]["rendered"],
                    description=description,
                    url=post["link"],
                )

                embed.set_footer(text=footer)

                await ctx.send(embed=embed)

            if more:
                more_embed = Embed(
                    title=_("See the full search results for `{}`.".format(query)),
                    url=search_url,
                )

                await ctx.send(embed=more_embed)
        except Exception as e:  # pylint: disable=broad-except
            await ctx.send(_("Sorry, an anknown error occurred:\n`{}`".format(e)))

    @commands.group("wpset")
    async def wpset(self, ctx):
        """
        Change WordPress settings.
        """

    @wpset.command(name="posttype")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def posttype(self, ctx: commands.Context, post_type=None):
        """
        Set which post type to search.
        Default = posts
        """

        await self.config.guild(ctx.guild).post_type.set(post_type)

        await ctx.send(_("The post type has been set to `{}`.").format(post_type))

    @wpset.command(name="showdate")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def showdate(self, ctx: commands.Context):
        """
        Toggle whether or not to display post dates.
        """

        confirm = await ctx.send(_("Display post dates?"))

        start_adding_reactions(confirm, ReactionPredicate.YES_OR_NO_EMOJIS)
        pred = ReactionPredicate.yes_or_no(confirm, ctx.author)

        try:
            await ctx.bot.wait_for("reaction_add", check=pred)
        except asyncio.TimeoutError:
            return await ctx.send(_("Timed out."))

        if pred.result:
            await self.config.guild(ctx.guild).show_date.set(True)

            return await ctx.send(_("Post date display enabled."))

        await self.config.guild(ctx.guild).show_date.set(False)

        return await ctx.send(_("Post date display disabled."))

    @wpset.command(name="max")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def max(self, ctx: commands.Context, max_posts=5):
        """
        Set how many search results should be displayed.
        Default = 5
        """

        await self.config.guild(ctx.guild).max_posts.set(max_posts)

        await ctx.send(
            _("The max number of results has been set to `{}`").format(max_posts)
        )

    @wpset.command(name="order")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def order(self, ctx: commands.Context, order="desc"):
        """
        Set post sort direction.
        Default = desc
        Valid = asc, desc
        """

        valid = [
            "asc",
            "desc",
        ]

        if order not in valid:
            return await ctx.send(
                _("The following option is invalid: `{}`!".format(order))
            )

        await self.config.guild(ctx.guild).order.set(order)

        await ctx.send(_("The sort direction has been set to `{}`.").format(order))

    @wpset.command(name="orderby")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def orderby(self, ctx: commands.Context, orderby="relevance"):
        """
        Set post sort order.
        Default = relevance
        Valid = ID, author, title, name, type, date, modified,
                rand, comment_count, relevance, menu_order
        """

        valid = [
            "ID",
            "author",
            "title",
            "name",
            "type",
            "date",
            "modified",
            "rand",
            "comment_count",
            "relevance",
            "menu_order",
        ]

        if orderby not in valid:
            return await ctx.send(
                _("The following option is invalid: `{}`!".format(orderby))
            )

        await self.config.guild(ctx.guild).orderby.set(orderby)

        await ctx.send(_("The sort order has been set to `{}`.").format(orderby))

    @wpset.command(name="url")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_messages=True)
    async def url(self, ctx: commands.Context, site_url=None):
        """
        Set the site URL.
        """

        await self.config.guild(ctx.guild).site_url.set(site_url)

        await ctx.send(_("The site URL has been set to `{}`.").format(site_url))
