class WordPressAPIError(Exception):
    pass


class WordPressInvalidParameters(WordPressAPIError):
    pass


class WordPressAPICooldown(WordPressAPIError):
    pass


class WordPressMissingAPIKey(WordPressAPIError):
    pass


class WordPressMissingSiteURL(WordPressAPIError):
    pass
